package com.example.ra811076.projetsqlite

object MobileCourseTable {
    val NAME = "MobileCourse"
    val ID = "_id"
    val TITLE = "title"
    val TIME = "time"
}